# Prerequisites:
- Install Node v10.15.3+ (https://nodejs.org/en/download/)
- npm install

# How to use

## Command:
node createDC.js --sc=<scenario_number> [--server=<name>] [-steps=<number>] [--subStpes=<1|2>] [--delay=<milliseconds>] [--mayAdd=<true|false>] [--addAttachedFile=<true|false>] [--cfWithout=<true|false>]

### Parameters:
--sc : follow with 1 - 12
--server : select sever to create DC at. Default is at cryptoblk Dev.
--steps : stop after step number finished. For example sceanrio 11, if steps=3, it will stop after confirming bank advise with confirmation
--subSteps:  1 after maker submit, 2 after checker verify
--delay=<Milli Seconds> : Delay between action
--mayAdd=<true|false> : Use MAY_ADD instead of WITH for confirmation.
--addAttachedFile=<true|false> : upload a test.pdf to DC
--cfWithout=<true|false> advise with confirmation or not

### Environment:
1. dev : cryptoblk dev
2. dev2 : cryptoblk dev2
3. dev3 : cryptoblk dev3
4. contour-dev1 : contour dev1
5. contour-dev2 : contour dev2
6. contour-dev3 : contour dev3
7. local : local server

## Create In-completed  DC:

### If you only want to do incomplete process, you can use --steps=<number>.

For example:
node createDC.js --sc=1 --steps=1
After run this command, an applicant only submit a draft of scenario#1 DC then stop
Create In-completed draft DC :

### If you only want to create DC after either maker submits or checker verify you can use steps and subSteps together

For example:

This will stop after applicant maker submits:

node createDC.js --sc=1 --steps=1 --subSteps=1

note that: node createDC.js --sc=1 --steps=1 this is still for applicant finished submit DC to Issuing Bank
Add Delay Between Each Node Action:

### If you only want to do have delay for each node action, you can use --delay=<milliseconds>.

node createDC.js --sc=1 --delay=3000
Switch Bank Ordering:

### In case, you want to swap to make BankA becoming BankB

Go to createDC.js

Looking for BANKA_NAME, BANKA_URL, BANKA_LOGIN, BANKB_NAME, BANKB_URL, and BANKB_LOGIN.

Switch data from BANKA to BANKB