const newman = require('newman');
const argv = require('yargs').argv
const voltronReqs = require('./voltron-requests.json');
const nodeSetupMap = require('./nodeSetup.json')

const fs = require('fs');

const newManCollection = require('./defaultCollection.json')
var regex = /dog/g;

const URL_NODE_NAME_REGEX = /NODE/g;
const LOGIN_NODE_NAME_REGEX = /corpa/g;
const LAST_SHIPMENT_DATE_REGEX = /LAST_SHIPMENT_DATE/g;
const EXPIRY_DATE_REGEX = /EXPIRY_DATE/g;
const CONFIRMING_BANK_REGEX = /CONFIRMING_BANK/g;
const SERVER_REGEX = /SERVER/g;
const PASSWORD_REGEX = /PASSWORD/g;
const USERNAME_REGEX = /USERNAME/g;

const CORP_A_REGEX = /CORPA/g;
const CORP_B_REGEX = /CORPB/g;

const BANK_A_REGEX = /BANKA/g;
const BANK_B_REGEX = /BANKB/g;
const BANK_C_REGEX = /BANKC/g;
const BANK_D_REGEX = /BANKD/g;

const DRAFT_UUID_REGEX = /DRAFTUUID/g;


const URL_NODE_NAME = "corp-a"
const LOGIN_NODE_NAME = "corpa"

const loginOpt = (!!argv.login) ? argv.login:"all";
const selectSC = (!!argv.sc) ? argv.sc:"1";
const stepNum = (!!argv.steps) ? argv.steps:1000;
const subStepNum = (!!argv.subSteps) ? argv.subSteps:1000;
const actionDelay = (!!argv.delay) ? argv.delay:3000;
const server = (!!argv.server) ? argv.server:"dev2";
const skipTo = (!!argv.skipTo) ? argv.skipTo:0;
const targetDCUUID = (!!argv.dcUUID) ? argv.dcUUID:"";
const mayAdd = (!!argv.mayAdd) ? argv.mayAdd == "true": false;
const cfWithout = (!!argv.cfWithout) ? argv.cfWithout == "true": false;
const savedID = (!!argv.savedID) ? argv.savedID: "NONE";
const outputFile = (!!argv.outputFile) ? argv.outputFile: "output.txt";
const prefixRef = (!!argv.prefixRef)? argv.prefixRef : "";
const attachedFileID = (!!argv.attachedFileID)? argv.attachedFileID : "";
const addAttachedFile = (!!argv.addAttachedFile)? argv.addAttachedFile : false;
const setName = (!!argv.setName)?argv.setName : "set1";
const protocal = (!!argv.protocal)? argv.protocal : "https";
const nodesMap = require('./env/'+server+'.json');
const setMap = nodeSetupMap[setName];

const lastShipmentDate = "2022-01-30"
const expiryDate = "2022-01-01"

const corpa = setMap["corp-a"];
const CORPA_NAME = nodesMap[corpa]["name"];
const CORPA_URL = corpa
const CORPA_LOGIN = corpa

const corpb = setMap["corp-b"];
const CORPB_NAME = nodesMap[corpb]["name"];
const CORPB_URL = corpb
const CORPB_LOGIN = corpb;

const banka = setMap["bank-a"];
const BANKA_NAME = nodesMap[banka]["name"];
const BANKA_URL = banka
const BANKA_LOGIN = banka;

const bankb = setMap["bank-b"];
const BANKB_NAME = nodesMap[bankb]["name"];
const BANKB_URL = bankb
const BANKB_LOGIN = bankb;

const bankc = setMap["bank-c"];
const BANKC_NAME = nodesMap[bankc]["name"];
const BANKC_URL = bankc
const BANKC_LOGIN = bankc;

const bankd = setMap["bank-d"];
const BANKD_NAME = nodesMap[bankd]["name"];
const BANKD_URL = bankd
const BANKD_LOGIN = bankd;

const confirmingBankX500 = BANKC_NAME


var currentCSRFTOKEN = ""
var curNewDCRef = ""
var curDraftUUID= ""
var curBodyRaw = ""
var curNewDCUUID = targetDCUUID;
var curAttachFileID = attachedFileID;

function replaceValue(src, keys, value)
{	
	var target = src
	for (let i = 0; i < keys.length - 1; i++) 
	{
		target = target[keys[i]];
	}
	target[keys[keys.length - 1]] = value;
}

function CloneRequestForNode(requestValue, nodeName, username, password)
{
	let nodeURL =  nodesMap[nodeName].url;
	var requestStr = JSON.stringify(requestValue, undefined, 2);
	requestStr = requestStr.replace(USERNAME_REGEX, username)
	requestStr = requestStr.replace(PASSWORD_REGEX, password)
	requestStr = requestStr.replace(EXPIRY_DATE_REGEX, expiryDate)
	requestStr = requestStr.replace(LAST_SHIPMENT_DATE_REGEX, lastShipmentDate)
	requestStr = requestStr.replace(CONFIRMING_BANK_REGEX, confirmingBankX500)
	requestStr = requestStr.replace(/DOMAIN_URL_FULL/g, nodeURL)

	requestStr = requestStr.replace(/PROTOCAL/g, protocal)

	requestStr = requestStr.replace(CORP_A_REGEX, CORPA_NAME)
	requestStr = requestStr.replace(CORP_B_REGEX, CORPB_NAME)

	requestStr = requestStr.replace(BANK_A_REGEX, BANKA_NAME)
	requestStr = requestStr.replace(BANK_B_REGEX, BANKB_NAME)
	requestStr = requestStr.replace(BANK_C_REGEX, BANKC_NAME)
	requestStr = requestStr.replace(BANK_D_REGEX, BANKD_NAME)

	let requestJSON = JSON.parse(requestStr);
	let hostList = requestJSON.request.url.host;
	nodeURL.split('.').forEach((value)=> {
		hostList.push( value.replace(/(http:\/\/)|(https:\/\/)/g,'') );
	})
	
	return requestJSON;
}

function getReponseCode(responseItem)
{
	return responseItem.PostmanResponse.code == '200'
}

function CreateApplyDC(scenario, nodeName, userName, password, mayAdd, attachments)
{
	let applyDCReqTmp = JSON.stringify(CloneRequestForNode(voltronReqs["applyDC"+scenario].request, nodeName, userName, password));
	if (mayAdd)
	{
		applyDCReqTmp = applyDCReqTmp.replace(/WITH/g, "MAY_ADD");
	}

	applyDCReqTmp = applyDCReqTmp.replace(/ATTACHED_FILES/g, attachments);

	return JSON.parse(applyDCReqTmp);
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

var stopProcess = false;
var countTest = 0;

function runNewMan(nodeName)
{
	newman.run({
	    collection: newManCollection,
	    bail: true,
	    reporters: ['json']
	}).on('request', function(err, item) {
		
		if (!!err)
		{
			stopProcess = true;
			console.log(err);
			return;
		}

		// item.cursor.position = -1;
		console.log("\n[REQUEST]"+nodeName, "command = "+item.item.name, item.response.code)
		if(item.response.code !== 200)
		{
			console.log(JSON.stringify(item.response.stream.toString(), undefined, 2));
			stopProcess = true;
			return;
		}

		let bodyJson = item.response.stream.toString();
		let responseText = bodyJson;
		if (!!bodyJson)
		{
			if(IsJsonString(bodyJson))
			{
				try 
				{
					bodyJson = JSON.parse(bodyJson);
				} 
				catch (e)
				{
					return;
				}
			}
		}

		if ( item.item.name === "login")
		{
			let headers = JSON.parse(JSON.stringify(item.response.headers));
			for ( let idx in headers )
			{	
				let headerItem = headers[idx];
				if (headerItem.key === 'X-CSRF-TOKEN' || headerItem.key === "x-csrf-token")
				{
					currentCSRFTOKEN = headerItem.value
				}
			}

		}
		else if (item.item.name === "applyDCS"+selectSC)
		{	
			curDraftUUID = bodyJson.id;
			curNewDCRef = bodyJson.id;
		}
		else if (item.item.name == "approveDraft")
		{
			curDraftUUID = bodyJson.id;
			curNewDCRef = bodyJson.id;
		}
		else if (item.item.name === "getAllDCs")
		{
			lcList = bodyJson.content;
			for (idx in lcList)
			{
				if(!!lcList[idx].locApp && lcList[idx].locApp.ref.search(curNewDCRef) >= 0)
				{
					curNewDCUUID = lcList[idx].uuid
				}
				else 
				{
					let lcStr = JSON.stringify(lcList[idx]);
					if(lcStr.search(curNewDCRef) >= 0)
					{
						curNewDCUUID = lcList[idx].uuid
					}					
				}
			}
		}
		else if (item.item.name === "beneApproveDC")
		{
			curDraftUUID = bodyJson.id;
		}
		else if (item.item.name == "issueDC")
		{
			curDraftUUID = bodyJson.id;
		}
		else if (item.item.name == "adviseDC" || item.item.name == "adviseDCWith" || item.item.name == "confirmDCWith" || item.item.name == "availableApproveDC")
		{
			curDraftUUID = bodyJson.id;
		}
		else if(item.item.name == "getDC")
		{
			if(selectSC != "findDraft")
			{
				curNewDCRef = bodyJson.ref
				curBodyRaw = bodyJson._data.props;
			}
		}
		else if(item.item.name == "uploadFile")
		{
			curAttachFileID = responseText;
			console.log("curAttachFileID = " + curAttachFileID);
		}
		
	}).on('beforeRequest',function(err, item) {
		if (item.item.name !== "login")
		{					
			var headerMembers = item.request.headers.members
			for( idx in headerMembers ) 
			{
				if( headerMembers[idx].key === "x-csrf-token" || headerMembers[idx].key === "X-CSRF-TOKEN")
				{
					headerMembers[idx].value = currentCSRFTOKEN;				
				}
			}
			if(item.item.name === "beneApproveDC")
			{
				item.request.url.path[4] =  curNewDCRef
				item.request.body.raw = item.request.body.raw.replace(/BODYRAW/g, JSON.stringify(curBodyRaw))
			}
			else if(item.item.name === "adviseDC" || item.item.name === "adviseDCWith" || item.item.name === "issueDC" || item.item.name == "availableApproveDC")
			{
				item.request.url.path[5] =  curNewDCRef
			}
			else if(item.item.name === "confirmDCWith")
			{				
				item.request.url.path[5] = curNewDCRef
			}
			else if (item.item.name == "approveDraft")
			{
				item.request.url.path[4] = curDraftUUID
			}
			else if(item.item.name.search("applyDCS") >= 0)
			{
				item.request.body.raw = item.request.body.raw.replace(/ATTACHED_FILE_ID/g, curAttachFileID);
				// console.log( JSON.stringify(item.request));
			}
		}
	}).on('done', function(err, item)
	{
		callNextAction()
	})
}

function getTimeID()
{
	let curDate = new Date()
	return curDate.getHours()+""+curDate.getMinutes()+""+curDate.getSeconds()
}

function corpbApproveDC()
{
	let nodeBInfo = nodesMap[CORPB_URL]

	newManCollection.item.splice(0, newManCollection.item.length)
	
	let loginReq1 = CloneRequestForNode(voltronReqs.login.request, CORPB_URL,nodeBInfo.username1, nodeBInfo.password1);
	// let loginReq2 = CloneRequestForNode(voltronReqs.login.request, CORPB_URL,nodeBInfo.username2, nodeBInfo.password2);

	let getDCReq = CloneRequestForNode(voltronReqs.getDC.request, CORPB_URL);
	let getDCReqStr = JSON.stringify(getDCReq)
	getDCReq  = JSON.parse(getDCReqStr.replace(/BBDCUUID/g, curNewDCUUID))
	
	let beneApproveDCReq = CloneRequestForNode(voltronReqs.beneApproveDC.request, CORPB_URL);
	let beneApproveDCStr = JSON.stringify(beneApproveDCReq)
	beneApproveDCStr = beneApproveDCStr.replace(/BBDCUUID/g, curNewDCUUID)
	beneApproveDCReq = JSON.parse(beneApproveDCStr)

	// var approveDraft = CloneRequestForNode(voltronReqs.approveDraft.request, CORPB_URL);

	newManCollection.item.push(loginReq1);
	newManCollection.item.push(getDCReq);
	newManCollection.item.push(beneApproveDCReq);
	// newManCollection.item.push(loginReq2);
	// newManCollection.item.push(approveDraft);

	runNewMan(CORPB_LOGIN)
}

function findDraftUUID()
{
	let nodeAInfo = nodesMap[CORPA_URL];
	newManCollection.item.splice(0, newManCollection.item.length);

	var loginReq = CloneRequestForNode(voltronReqs.login.request, CORPA_URL, nodeAInfo.username0, nodeAInfo.password0);
	let getDCReq = CloneRequestForNode(voltronReqs.getDC.request, CORPA_URL);
	getDCReqStr = JSON.stringify(getDCReq)
	getDCReq  = JSON.parse(getDCReqStr.replace(/BBDCUUID/g, targetDCUUID))

	
	newManCollection.item.push( loginReq );
	newManCollection.item.push( getDCReq );

	runNewMan(CORPA_LOGIN);
}

function uploadFile()
{
	let nodeAInfo = nodesMap[CORPA_URL];
	newManCollection.item.splice(0, newManCollection.item.length);

	var loginReq = CloneRequestForNode(voltronReqs.login.request, CORPA_URL, nodeAInfo.username0, nodeAInfo.password1);
	let loadFileReq = CloneRequestForNode(voltronReqs.uploadFile.request, CORPA_URL);
	let loadFileReqTmp = JSON.stringify(loadFileReq);
	loadFileReqTmp.replace(/FILE_NAME/g, "test.pdf")
	loadFileReq = JSON.parse(loadFileReqTmp);
	
	newManCollection.item.push( loginReq );
	newManCollection.item.push( loadFileReq );

	runNewMan(CORPA_LOGIN);
}

function corpaCreateDC()
{
	let nodeAInfo = nodesMap[CORPA_URL]
	newManCollection.item.splice(0, newManCollection.item.length)

	let attachedDiplayName = "S"+selectSC+"_"+getTimeID()+".pdf";
	
	let attachments = (addAttachedFile)?"{\\\"name\\\":\\\""+attachedDiplayName+"\\\",\\\"id\\\":\\\"ATTACHED_FILE_ID\\\"}" : "";

	let loadFileReq = CloneRequestForNode(voltronReqs.uploadFile.request, CORPA_URL);
	let loadFileReqTmp = JSON.stringify(loadFileReq);
	// loadFileReqTmp.replace(/FILE_NAME/g, "test.pdf")
	loadFileReq = JSON.parse(loadFileReqTmp);

	// let attachments = "";
	if(tempCurActionIdx != stepNum)
	{
		var loginReq = CloneRequestForNode(voltronReqs.login.request, CORPA_URL, nodeAInfo.username0, nodeAInfo.password1);
		var applyDCReq = CreateApplyDC("S"+selectSC, CORPA_URL, nodeAInfo.username1, nodeAInfo.password0, mayAdd, attachments);
		
		newManCollection.item.push( loginReq );
		// console.log("addAttachedFile", addAttachedFile);
		if(addAttachedFile)
		{
			// console.log("11 do load file req");
			newManCollection.item.push( loadFileReq );
		}

		console.log(JSON.stringify(applyDCReq, undefined, 2));
		newManCollection.item.push( applyDCReq );
	}
	else
	{
		let countDownSteps = subStepNum;

		var loginReq = CloneRequestForNode(voltronReqs.login.request, CORPA_URL, nodeAInfo.username1, nodeAInfo.password1);
		var applyDCReq = CreateApplyDC("S"+selectSC, CORPA_URL, nodeAInfo.username1, nodeAInfo.password1, mayAdd, attachments);
	
		var loginReq2 = CloneRequestForNode(voltronReqs.login.request, CORPA_URL, nodeAInfo.username2, nodeAInfo.password2);
		var approveDraft2 = CloneRequestForNode(voltronReqs.approveDraft.request, CORPA_URL, nodeAInfo.username2, nodeAInfo.password2);

		var loginReq3 = CloneRequestForNode(voltronReqs.login.request, CORPA_URL, nodeAInfo.username3, nodeAInfo.password3);
		var approveDraft3 = CloneRequestForNode(voltronReqs.approveDraft.request, CORPA_URL, nodeAInfo.username3, nodeAInfo.password3);
		
		newManCollection.item.push( loginReq );
		// console.log("addAttachedFile", addAttachedFile);
		if(addAttachedFile)
		{
			// console.log("222 do load file req");
			newManCollection.item.push( loadFileReq );
		}
		newManCollection.item.push( applyDCReq );
		
		countDownSteps--;
		if(countDownSteps > 0)
		{
			newManCollection.item.push( loginReq2 );
			newManCollection.item.push( approveDraft2);

			countDownSteps--;
			if(countDownSteps >0)
			{
				newManCollection.item.push( loginReq3 );
				newManCollection.item.push( approveDraft3);
			}
		}
	}

	const getAllDCReq = CloneRequestForNode(voltronReqs.listDCs.request, CORPA_URL, nodeAInfo.username1, nodeAInfo.password1);
	newManCollection.item.push( getAllDCReq );

	runNewMan(CORPA_LOGIN)
}


function bankAIssueDC()
{
	let bankAInfo = nodesMap[BANKA_URL]
	newManCollection.item.splice(0, newManCollection.item.length)
	let issueDCID = prefixRef+"S"+selectSC+"ISDC"+getTimeID();

	let issueDCReq = CloneRequestForNode(voltronReqs.issueDC.request, BANKA_URL);

	let issueDCReqStr = JSON.stringify(issueDCReq)
	issueDCReqStr = issueDCReqStr.replace(/BBDCUUID/g, curNewDCUUID)
	issueDCReqStr = issueDCReqStr.replace("BBISDCNUMBER", issueDCID)
	issueDCReq = JSON.parse(issueDCReqStr)

	if(tempCurActionIdx != stepNum)
	{
		let loginReq = CloneRequestForNode(voltronReqs.login.request, BANKA_URL, bankAInfo.username0, bankAInfo.password0);
		let getDCReq = CloneRequestForNode(voltronReqs.getDC.request, BANKA_URL);
		getDCReqStr = JSON.stringify(getDCReq)
		getDCReq  = JSON.parse(getDCReqStr.replace(/BBDCUUID/g, curNewDCUUID))

		newManCollection.item.push( loginReq );
		newManCollection.item.push( getDCReq );
		newManCollection.item.push( issueDCReq );
	}
	else 
	{
		let countDownSteps = subStepNum;
		let loginReq1 = CloneRequestForNode(voltronReqs.login.request, BANKA_URL, bankAInfo.username1, bankAInfo.password1);
		let loginReq2 = CloneRequestForNode(voltronReqs.login.request, BANKA_URL, bankAInfo.username2, bankAInfo.password2);
		let loginReq3 = CloneRequestForNode(voltronReqs.login.request, BANKA_URL, bankAInfo.username3, bankAInfo.password3);
	
		let getDCReq = CloneRequestForNode(voltronReqs.getDC.request, BANKA_URL);
		getDCReqStr = JSON.stringify(getDCReq)
		getDCReq  = JSON.parse(getDCReqStr.replace(/BBDCUUID/g, curNewDCUUID))
	
		let issueDCReq = CloneRequestForNode(voltronReqs.issueDC.request, BANKA_URL);
	
		let issueDCReqStr = JSON.stringify(issueDCReq)
		// issueDCReqStr = issueDCReqStr.replace(/BBDCID/g, curNewDCRef)
		issueDCReqStr = issueDCReqStr.replace(/BBDCUUID/g, curNewDCUUID)
		issueDCReqStr = issueDCReqStr.replace("BBISDCNUMBER", issueDCID)
	
		issueDCReq = JSON.parse(issueDCReqStr)
	
		let approveDraft = CloneRequestForNode(voltronReqs.approveDraft.request, BANKA_URL);
	
		newManCollection.item.push( loginReq1 );
		newManCollection.item.push( getDCReq );
		newManCollection.item.push( issueDCReq );

		countDownSteps--;
		if(countDownSteps > 0)
		{
			newManCollection.item.push( loginReq2 );
			newManCollection.item.push( approveDraft );
		}

		countDownSteps--;
		if(countDownSteps > 0)
		{
			newManCollection.item.push( loginReq3 );
			newManCollection.item.push( approveDraft );
		}
		
	}
	
	runNewMan(BANKA_LOGIN)
}

function bankBAdviseDC()
{
	let bankBInfo = nodesMap[BANKB_URL]
	newManCollection.item.splice(0, newManCollection.item.length);

	let getDCReq = CloneRequestForNode(voltronReqs.getDC.request, BANKB_URL);
	getDCReqStr = JSON.stringify(getDCReq)
	getDCReq  = JSON.parse(getDCReqStr.replace(/BBDCUUID/g, curNewDCUUID))

	let adviseDCWithReq = CloneRequestForNode(voltronReqs.adviseDCWith.request, BANKB_URL);
	let adviseDCWithID = prefixRef+"S"+selectSC+"ADDC"+getTimeID();

	let adviseDCWithReqStr = JSON.stringify(adviseDCWithReq)
	adviseDCWithReqStr = adviseDCWithReqStr.replace(/BBDCUUID/g, curNewDCUUID)
	adviseDCWithReqStr = adviseDCWithReqStr.replace("BBADDCNUMBER", adviseDCWithID)
	adviseDCWithReq = JSON.parse(adviseDCWithReqStr)

	let approveDraft = CloneRequestForNode(voltronReqs.approveDraft.request, BANKB_URL);

	if(tempCurActionIdx != stepNum)
	{
		let loginReq = CloneRequestForNode(voltronReqs.login.request, BANKB_URL, bankBInfo.username0, bankBInfo.password0);
		let getDCReq = CloneRequestForNode(voltronReqs.getDC.request, BANKB_URL);
		getDCReqStr = JSON.stringify(getDCReq)
		getDCReq  = JSON.parse(getDCReqStr.replace(/BBDCUUID/g, curNewDCUUID))
		
		newManCollection.item.push( loginReq);
		newManCollection.item.push( getDCReq );
		newManCollection.item.push( adviseDCWithReq );
	}
	else
	{
		let loginReq1 = CloneRequestForNode(voltronReqs.login.request, BANKB_URL, bankBInfo.username1, bankBInfo.password1);
		let loginReq2 = CloneRequestForNode(voltronReqs.login.request, BANKB_URL, bankBInfo.username2, bankBInfo.password2);
		let loginReq3 = CloneRequestForNode(voltronReqs.login.request, BANKB_URL, bankBInfo.username3, bankBInfo.password3);
			
		
		newManCollection.item.push( loginReq1);
		newManCollection.item.push( getDCReq );
		newManCollection.item.push( adviseDCWithReq );
		
		let countDownSteps = subStepNum;
		countDownSteps--;
		if(countDownSteps > 0)
		{
			newManCollection.item.push( loginReq2 );
			newManCollection.item.push( approveDraft );
		}

		countDownSteps--;
		if(countDownSteps > 0)
		{
			newManCollection.item.push( loginReq3 );
			newManCollection.item.push( approveDraft );
		}
	}	

	runNewMan(BANKB_LOGIN)
}


function bankCConfirmDCWith()
{
	// 
	let bankCInfo = nodesMap[BANKC_URL]
	let urlName = BANKC_URL

	newManCollection.item.splice(0, newManCollection.item.length);

	let getDCReq = CloneRequestForNode(voltronReqs.getDC.request, urlName);
	getDCReqStr = JSON.stringify(getDCReq)
	getDCReq  = JSON.parse(getDCReqStr.replace(/BBDCUUID/g, curNewDCUUID))

	let confirmDCWithReq = CloneRequestForNode(voltronReqs.confirmDCWith.request, urlName);
	let confirmDCID = prefixRef+"S"+selectSC+"CFDC"+getTimeID();

	let confirmDCWithReqStr = JSON.stringify(confirmDCWithReq)
	confirmDCWithReqStr = confirmDCWithReqStr.replace(/BBDCUUID/g, curNewDCUUID)
	confirmDCWithReqStr = confirmDCWithReqStr.replace("BBCFDCNUMBER", confirmDCID)
	if(cfWithout)
	{
		confirmDCWithReqStr = confirmDCWithReqStr.replace(/CONFIRMATION_METHOD/g, "withoutConfirmation");
	}
	else {
		confirmDCWithReqStr = confirmDCWithReqStr.replace(/CONFIRMATION_METHOD/g, "withConfirmation");
	}
	
	confirmDCWithReq = JSON.parse(confirmDCWithReqStr)
	// console.log(JSON.stringify(confirmDCWithReq, undefined, 2));

	let approveDraft = CloneRequestForNode(voltronReqs.approveDraft.request, urlName);

	if(tempCurActionIdx != stepNum) 
	{
		let loginReq = CloneRequestForNode(voltronReqs.login.request, urlName, bankCInfo.username0, bankCInfo.password0);
		newManCollection.item.push( loginReq );
		newManCollection.item.push( getDCReq );
		newManCollection.item.push( confirmDCWithReq );
	}
	else 
	{
		let loginReq1 = CloneRequestForNode(voltronReqs.login.request, urlName, bankCInfo.username1, bankCInfo.password1);
		let loginReq2 = CloneRequestForNode(voltronReqs.login.request, urlName, bankCInfo.username2, bankCInfo.password2);
		let loginReq3 = CloneRequestForNode(voltronReqs.login.request, urlName, bankCInfo.username3, bankCInfo.password3);

		newManCollection.item.push( loginReq1 );
		newManCollection.item.push( getDCReq );
		newManCollection.item.push( confirmDCWithReq );

		let countDownSteps = subStepNum;
		countDownSteps--;
		if(countDownSteps > 0)
		{
			newManCollection.item.push( loginReq2 );
			newManCollection.item.push( approveDraft );
		}

		countDownSteps--;
		if(countDownSteps > 0)
		{
			newManCollection.item.push( loginReq3 );
			newManCollection.item.push( approveDraft );
		}
	}

	runNewMan(BANKC_LOGIN)	
}

function bankDConfirmDCWith()
{
	// 
	let urlName = BANKD_URL
	let loginName = BANKD_LOGIN

	newManCollection.item.splice(0, newManCollection.item.length)

	let loginReq = CloneRequestForNode(voltronReqs.login.request, urlName);
	// loginReq = setUpLoginUser(loginReq, loginName)


	let getDCReq = CloneRequestForNode(voltronReqs.getDC.request, urlName);
	getDCReqStr = JSON.stringify(getDCReq)
	getDCReq  = JSON.parse(getDCReqStr.replace(/BBDCUUID/g, curNewDCUUID))

	let confirmDCWithReq = CloneRequestForNode(voltronReqs.confirmDCWith.request, urlName);
	let confirmDCID = prefixRef+"S"+selectSC+"CFDC"+getTimeID();

	let confirmDCWithReqStr = JSON.stringify(confirmDCWithReq)
	confirmDCWithReqStr = confirmDCWithReqStr.replace(/BBDCUUID/g, curNewDCUUID)
	confirmDCWithReqStr = confirmDCWithReqStr.replace("BBCFDCNUMBER", confirmDCID)
	confirmDCWithReq = JSON.parse(confirmDCWithReqStr)

	newManCollection.item.push( loginReq );
	newManCollection.item.push( getDCReq );
	newManCollection.item.push( confirmDCWithReq );

	runNewMan(loginName)	
}


function bankDAVAdviseDC()
{
	
	let bankCInfo = nodesMap[BANKD_URL]
	let urlName = BANKD_URL

	newManCollection.item.splice(0, newManCollection.item.length)

	let getDCReq = CloneRequestForNode(voltronReqs.getDC.request, urlName);
	getDCReqStr = JSON.stringify(getDCReq);
	getDCReq  = JSON.parse(getDCReqStr.replace(/BBDCUUID/g, curNewDCUUID));

	let availableAdviseDCWithReq = CloneRequestForNode(voltronReqs.availableApproveDC.request, urlName);
	let availableAdviseDCID = prefixRef+"S"+selectSC+"AVDC"+getTimeID();

	let availableAdviseDCWithReqStr = JSON.stringify(availableAdviseDCWithReq);
	availableAdviseDCWithReqStr = availableAdviseDCWithReqStr.replace(/BBDCUUID/g, curNewDCUUID);
	availableAdviseDCWithReqStr = availableAdviseDCWithReqStr.replace("BBAVDCNUMBER", availableAdviseDCID);
	availableAdviseDCWithReq = JSON.parse(availableAdviseDCWithReqStr);

	let approveDraft = CloneRequestForNode(voltronReqs.approveDraft.request, urlName);

	if(tempCurActionIdx != stepNum)
	{
		let loginReq = CloneRequestForNode(voltronReqs.login.request, urlName, bankCInfo.username0, bankCInfo.password0);
		newManCollection.item.push( loginReq );
		newManCollection.item.push( getDCReq );
		newManCollection.item.push( availableAdviseDCWithReq );
	}
	else 
	{
		let loginReq1 = CloneRequestForNode(voltronReqs.login.request, urlName, bankCInfo.username1, bankCInfo.password1);
		let loginReq2 = CloneRequestForNode(voltronReqs.login.request, urlName, bankCInfo.username2, bankCInfo.password2);	
		let loginReq3 = CloneRequestForNode(voltronReqs.login.request, urlName, bankCInfo.username3, bankCInfo.password3);	

		newManCollection.item.push( loginReq1 );
		newManCollection.item.push( getDCReq );
		newManCollection.item.push( availableAdviseDCWithReq );

		let countDownSteps = subStepNum;
		countDownSteps--;
		if(countDownSteps > 0)
		{
			newManCollection.item.push( loginReq2 );
			newManCollection.item.push( approveDraft );
		}

		countDownSteps--;
		if(countDownSteps > 0)
		{
			newManCollection.item.push( loginReq3 );
			newManCollection.item.push( approveDraft );
		}
	}

	runNewMan(BANKD_LOGIN)
}

function callNextAction(sucessCB)
{
	if (stopProcess)
	{
		console.log("NEW DC UDID = ", curNewDCUUID);
		return;
	}

	if ( curActionIdx <= stepNum)
	{
		tempCurActionIdx = curActionIdx;
		if( actionList[curActionIdx-1] === "corpbApproveDC")
		{
			setTimeout(corpbApproveDC, actionDelay)
		}
		else if(actionList[curActionIdx-1] == "corpbApproveDC")
		{
			setTimeout(corpbApproveDC, actionDelay)
		}
		else if(actionList[curActionIdx-1] === "bankAIssueDC")
		{
			setTimeout(bankAIssueDC, actionDelay)
		}
		else if(actionList[curActionIdx-1] === "bankBAdviseDC")
		{
			setTimeout(bankBAdviseDC, actionDelay)
		}
		else if(actionList[curActionIdx-1] === "bankBAdviseDCWith")
		{
			setTimeout(bankBAdviseDCWith, actionDelay)
		}
		else if (actionList[curActionIdx-1] === "bankCConfirmDCWith")
		{
			setTimeout(bankCConfirmDCWith, actionDelay)
		}
		else if (actionList[curActionIdx-1] === "bankDConfirmDCWith")
		{
			setTimeout(bankDConfirmDCWith, actionDelay)
		}
		else if (actionList[curActionIdx-1] === "bankDAvailableAdviseDC")
		{
			setTimeout(bankDAVAdviseDC, actionDelay)
		}
		else if(actionList[curActionIdx-1] === "corpaCreateDC")
		{
			setTimeout(corpaCreateDC, 1000)
		}
		else 	
		{
			console.log("NEW DC UDID = ", curNewDCUUID);
			// callNextActionInLoop()
			// sucessCB()
		}

		curActionIdx++;
	}
	else 
	{
		
		//save to file
		console.log("NEW DC UDID = ", curNewDCUUID);
		fs.appendFile(outputFile, savedID +"\t"+curNewDCUUID+"\r\n", (err) => {
			// throws an error, you could also catch it here
			if (err) throw err;
		
			// success case, the file was saved
		});
	}
}



var actionList = []
var curActionIdx = 1
var tempCurActionIdx = 1

if (curNewDCUUID != "")
{
}

switch(selectSC)
{

	case "test":
	{
		uploadFile();
	}
	break;
	case "findDraft":
	{
		findDraftUUID();
	}
	break;
	//S1
    // Issuing Bank = Asia Best Bank
	// Advising Bank = Mid-Atlantic Bank
	// Available with = Any (leave bank as blank)
	// Confirmation Instructions = Without

	//S2
	// Issuing Bank = Asia Best Bank
	// Advising Bank = Mid-Atlantic Bank
	// Available with = Mid-Atlantic Bank
	// Confirmation Instructions = Without

	//S3
	// Issuing Bank = Asia Best Bank
	// Advising Bank = Mid-Atlantic Bank
	// Available with = Asia Best Bank
	// Confirmation Instructions = Without
	case 1:
	case 2:
	case 3:
	{
		actionList.push("corpaCreateDC")
		actionList.push("bankAIssueDC")
		actionList.push("bankBAdviseDC")
	}
	break;

	case 4:
	{
	    // Issuing Bank = Asia Best Bank
		// Advising Bank = Mid-Atlantic Bank
		// Available with = Any (leave bank as blank)
	    // Confirmation Instructions = With / May add
		// Requested Confirmation Party = Mid-Atlantic Bank

		actionList.push("corpaCreateDC")
		actionList.push("bankAIssueDC")
		actionList.push("bankBAdviseDC")
	}
	break;

	case 5: //Not test
	{
	    // Issuing Bank = Asia Best Bank
    	// Advising Bank = Mid-Atlantic Bank
	    // Available with = Any (leave bank as blank)
	    // Confirmation Instructions = With / May add
	    // Requested Confirmation Party = Thorn Valley Bank

		actionList.push("corpaCreateDC")
		actionList.push("bankAIssueDC")
		actionList.push("bankCConfirmDCWith")
		actionList.push("bankBAdviseDC")
	}
	break;

	case 6: //Not Test
	{
		//Issuing Bank = Asia Best Bank
	 	//Advising Bank = Mid-Atlantic Bank
	 	//Available with = Mid-Atlantic Bank
	 	//Confirmation Instructions = With / May add
	 	//Requested Confirmation Party = Mid-Atlantic Bank

	 	actionList.push("corpaCreateDC")
		actionList.push("bankAIssueDC")
		actionList.push("bankBAdviseDC")
	}
	break;

	case 8:
	{
		//Issuing Bank = Asia Best Bank
	 	//Advising Bank = Mid-Atlantic Bank
	 	//Available with = Mid-Atlantic Bank
	 	//Confirmation Instructions = "With" / "May add"
	 	//Requested Confirmation Party = Thorn Valley Bank	 	

	 	actionList.push("corpaCreateDC")
		actionList.push("bankAIssueDC")
		actionList.push("bankCConfirmDCWith")
		actionList.push("bankBAdviseDC")
	}
	break;

	case 10:
	{
		//Issuing Bank = Asia Best Bank
  		//Advising Bank = Mid-Atlantic Bank
  		//Available with = Thorn Valley Bank
  		//Confirmation Instructions = "With" or "May add"
  		//Requested Confirmation Party = Thorn Valley Bank

  		actionList.push("corpaCreateDC")
		actionList.push("bankAIssueDC")
		actionList.push("bankCConfirmDCWith")
		actionList.push("bankBAdviseDC")
	}
	break;

	case 11:
	{
		//Issuing Bank = Asia Best Bank
  		//Advising Bank = Mid-Atlantic Bank
  		//Available with = Ferry Bank
  		//Confirmation Instructions = "With" or "May add"
  		//Requested Confirmation Party = Thorn Valley Bank

  		actionList.push("corpaCreateDC")
		actionList.push("bankAIssueDC")
		actionList.push("bankCConfirmDCWith")
		actionList.push("bankDAvailableAdviseDC");
		actionList.push("bankBAdviseDC")
	}
	break;

	case 12:
	{
		//Issuing Bank = Asia Best Bank
		//Advising Bank = Mid-Atlantic Bank
		//Available with = Ferry Bank
		//Confirmation Instructions = "Without"
		actionList.push("corpaCreateDC")
		actionList.push("bankAIssueDC")
		actionList.push("bankDAvailableAdviseDC");
		actionList.push("bankBAdviseDC")
	}
	break;


	default:
	{
	}
	break;
}


for(let idx=1; idx < skipTo; ++idx)
{
	actionList.shift();
}


// var countLoop = 0;
// function callNextActionInLoop()
// {
// 	if (countLoop < 20)
// 	{
// 		countLoop++
// 		curActionIdx = 0
// 		currentCSRFTOKEN = ""
// 		curNewDCRef = ""
// 		curNewDCUUID = ""
// 		callNextAction()
// 	}		
// }	

callNextAction()

// callNextActionInLoop()



