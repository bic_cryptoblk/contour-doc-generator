
const argv = require('yargs').argv
const { exec } = require("child_process");

const batchFile = (!!argv.batchFile) ? argv.batchFile:false;
if(!!!batchFile)
{
    console.log("Please selection batchFile to process by adding --batchFile=<filename>");
    return;
}

const voltronReqs = require("./"+batchFile);
const server = (!!argv.server) ? argv.server:"dev2";
const copyNum = (!!argv.copyNum) ? argv.copyNum:1;
const addAttachedFile = (!!argv.addAttachedFile) ? argv.addAttachedFile:false;
const domainURLFull = (!!argv.domainURL)? argv.domainURL : "voltron.solutions";
const protocal = (!!argv.protocal)? argv.protocal : "https";


function execShellCommand(cmd) {
    const exec = require('child_process').exec;
    return new Promise((resolve, reject) => {
     exec(cmd, (error, stdout, stderr) => {
      if (error) {
       console.warn(error);
      }
      resolve(stdout? stdout : stderr);
     });
    });
   }

// console.log(voltronReqs);
async function asyncForEach(array, callback) 
{
    for (let index = 0; index < array.length; index++) 
    {
      await callback(array[index], index, array);
    }
}

asyncForEach(voltronReqs, async(element) => {
    let scNo = element["sc"];
    let steps = element["steps"];
    let subSteps = element["subSteps"];
    let name = element["name"];
    let cfWithout = element["cfWithout"];

    let command = "node createDC.js --sc="+scNo;
    if(!!steps)
    {
        command += " --steps="+steps;
    }

    if(!!subSteps)
    {
        command += " --subSteps="+subSteps;
    }

    if(!!name)
    {
        command += " --savedID="+name;
    }

    if(!!addAttachedFile)
    {
        command += " --addAttachedFile=true";
    }

    if(!!cfWithout)
    {
        command += " --cfWithout=true";
    }

    command += " --server="+server;
    command += " --protocal="+protocal;
    command += " --domainURL="+domainURLFull;

    for(let idx=0; idx < copyNum; ++idx)
    {
        console.log("Copy#"+(idx+1)+" = " +command);
        await execShellCommand(command);
    }
});
